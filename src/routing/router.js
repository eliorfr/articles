import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer, DefaultTheme as NavDefaultTheme } from '@react-navigation/native';
import { navigationRef, onStateChange } from '../utils/navigation_service.js';

import MainTabs from './main_tabs';
import Home from '../screens/home.js';
import { useHeaderStyle } from '../hooks/use_header_style.js';
import HeaderTitle from '../components/header_title.js';
import UserDetails from '../components/user_details.js';
import HeaderBackButton from '../components/header_back_button.js';
import FullArticle from '../components/full_article.js';

const navigationTheme = {
  ...NavDefaultTheme,
  colors: {
    ...NavDefaultTheme.colors,
    background: 'white',
  },
};

const Router = () => {
  const Stack = createStackNavigator();
  const { headerStyles } = useHeaderStyle();

  return (
    <NavigationContainer ref={navigationRef} theme={navigationTheme} onStateChange={onStateChange}>
      <Stack.Navigator initialRouteName="MainTabs" screenOptions={{ gestureEnabled: false }}>
        {/* <Stack.Screen name="MainTabs" children={MainTabs} options={{ headerShown: false }} /> */}
        <Stack.Screen
          name="Home"
          component={Home}
          options={{
            headerStyle: headerStyles.header,
            headerTitleAlign: 'center',
            headerTitle: props => <HeaderTitle props={props} title="Articles" />,
            headerLeft: null,
          }}
        />
        <Stack.Screen
          name="FullArticle"
          component={FullArticle}
          options={{
            headerStyle: headerStyles.header,
            headerTitleAlign: 'center',
            headerTitle: props => <HeaderTitle props={props} title="Article" />,
            headerLeft: props => <HeaderBackButton props={props} />,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default React.memo(Router);
