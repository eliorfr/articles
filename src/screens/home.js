import React, { useCallback, useEffect, useState } from 'react';
import { View, FlatList } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';
import ArticleSearch from '../components/artical_search';
import Sections from '../components/sections';
import axios from 'axios';

import { useReduxState } from '../hooks/use_redux_state';
import Article from '../components/article';
import Footer from '../components/home_footer';

const styles = EStylesheet.create({
  container: {
    flex: 1,
  },
  flatList: {
    flex: 1,
  },
  flatListContentContainer: {
    flexGrow: 1,
    padding: 24,
  },
});

const Home = () => {
  const [headlines, setHeadlines] = useState([]);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState([]);
  const [selectedCategory] = useReduxState({ key: 'selectedCategory' });
  const [searchQuery] = useReduxState({ key: 'searchQuery' });
  console.log(selectedCategory);
  console.log(searchQuery);

  const getHeadlines = useCallback(async () => {
    const apiKey = 'bf1b3fad2ab149a896df0d0df0f1f021';
    const baseUrl = 'https://newsapi.org/v2/everything';
    setLoading(true);
    try {
      const url = `${baseUrl}?q=${searchQuery}${selectedCategory && `&category:${selectedCategory}`}&apiKey=${apiKey}`;
      const response = await fetch(url);
      const data = await response.json();
      setHeadlines(data.articles);
      console.log(data);
    } catch (error) {
      console.log(error);
      setHeadlines(null);
    } finally {
      setLoading(false);
    }
  }, [setHeadlines, selectedCategory, searchQuery]);

  useEffect(() => {
    getHeadlines();
  }, [getHeadlines]);

  console.log(headlines?.[0]);

  return (
    <View style={styles.container}>
      <ArticleSearch />
      <Sections />
      <FlatList
        style={styles.flatList}
        contentContainerStyle={styles.flatListContentContainer}
        data={headlines}
        keyExtractor={item => item.description}
        renderItem={({ item: article }) => <Article article={article} />}
        ListFooterComponent={() => <>{headlines && <Footer loading={loading} getData={getHeadlines} />}</>}
      />
    </View>
  );
};

export default Home;
