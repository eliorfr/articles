import { Keyboard } from 'react-native';
import { useEffect, useState } from 'react';

export const useKeyboard = deps => {
  const [isKeyboardShow, setIsKeyboardShow] = useState();
  const [top, setTop] = useState(0);
  const [height, setHeight] = useState(0);

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', ({ endCoordinates }) => {
      setTop(endCoordinates.screenY);
      setHeight(endCoordinates.height);

      setIsKeyboardShow(true);
    });
    Keyboard.addListener('keyboardDidHide', () => {
      setIsKeyboardShow(false);
    });

    return () => {
      Keyboard.removeListener('keyboardDidShow', () => {});
      Keyboard.removeListener('keyboardDidHide', () => {});
    };
  }, [deps]);

  return {
    top,
    isKeyboardShow,
    height,
    dismiss: Keyboard.dismiss,
  };
};
