import React, { useLayoutEffect } from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import { useNavigation } from '@react-navigation/core';

import HeaderBackButton from '../molecules/header_back_button';
import HeaderFileButton from '../molecules/header_file_buttons';
import { useHeaderStyle } from './use_header_style';
import { Dimensions } from 'react-native';

const DEVICE_WIDTH = Dimensions.get('screen').width;
const BUTTON_WIDTH = 55;
const TITLE_WIDTH = DEVICE_WIDTH - BUTTON_WIDTH * 4;

const styles = EStyleSheet.create({
  headerTitleContainer: {
    right: 50,
  },
  headerTitle: {
    width: TITLE_WIDTH,
    fontSize: 18,
    fontFamily: '$fontFamilyMedium',
  },
});

export const useFilesHeader = (title, url) => {
  const { headerStyles } = useHeaderStyle();
  const navigation = useNavigation();

  useLayoutEffect(() => {
    navigation.setOptions({
      headerStyle: headerStyles.header,
      headerTitleAlign: 'center',
      headerTitleStyle: styles.headerTitle,

      headerTitleContainerStyle: styles.headerTitleContainer,
      title,
      headerLeft: props => <HeaderBackButton props={props} />,
      headerRight: props => <HeaderFileButton props={props} url={url} />,
    });
  }, [headerStyles.header, navigation, title, url]);
};
