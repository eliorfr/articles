import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import dayjs from 'dayjs';

import { updateUserInfo } from '../actions/auth';
import { useReduxState } from './use_redux_state';

export const useUserData = () => {
  const dispatch = useDispatch();
  const [user] = useReduxState({ key: 'user' });
  const userId = user?.userId;
  const [accessToken] = useReduxState({ key: 'accessToken' });
  const [accessTokenExpiredAt] = useReduxState({ key: 'accessTokenExpiredAt' });

  useEffect(() => {
    let handler;
    if (userId && accessToken) {
      let interval = Math.abs(dayjs().diff(accessTokenExpiredAt?.clone().subtract(200, 'second'), 'second')) * 1000;
      interval = Math.min(interval, 1000 * 60);
      handler = setInterval(() => dispatch(updateUserInfo()), interval);
    } else {
      clearInterval(handler);
    }
    return () => {
      clearInterval(handler);
    };
  }, [userId, accessToken, accessTokenExpiredAt, dispatch]);
};
