import analytics from '@react-native-firebase/analytics';
import { useRoute } from '@react-navigation/core';
import { useCallback } from 'react';

export const useLogEvent = () => {
  const route = useRoute();
  const logEvent = useCallback(
    (name, params = {}) => {
      params.screen = route.name;
      return analytics().logEvent(name, params);
    },
    [route.name],
  );
  return logEvent;
};
