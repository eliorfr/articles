import { useDispatch, useSelector } from 'react-redux';
import { useCallback } from 'react';

export const useReduxState = ({ key }) => {
  const dispatch = useDispatch();
  const value = useSelector(s => s[key]);
  const setValue = useCallback(newValue => dispatch({ type: key, payload: newValue }), [dispatch, key]);
  return [value, setValue];
};
