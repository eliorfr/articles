import { useSafeAreaInsets } from 'react-native-safe-area-context';

export const useHeaderStyle = () => {
  const insets = useSafeAreaInsets();
  const headerStyles = {
    header: {
      height: 80 + insets.top,
      borderWidth: 0,
      elevation: 0,
      shadowOpacity: 0,
    },
  };

  return {
    headerStyles,
  };
};
