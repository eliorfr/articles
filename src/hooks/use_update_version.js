import { useCallback, useEffect, useRef } from 'react';
import { Platform, AppState } from 'react-native';
import { getVersion } from 'react-native-device-info';
import compareVersions from 'compare-versions';
import axios from 'axios';
import InAppUpdates, { IAUUpdateKind as UpdateKind } from 'sp-react-native-in-app-updates';

export const useUpdateVersion = () => {
  const getShouldForceUpdate = async () => {
    const { data: serverVersions } = await axios.get('/app_info/versions/');
    const appMinVersion = Platform.OS === 'android' ? serverVersions.appMinVersionAndroid : serverVersions.appMinVersionIos;
    const currentVersion = getVersion();
    return compareVersions(appMinVersion, currentVersion) === 1;
  };

  const configVersion = useCallback(async () => {
    const inAppUpdates = new InAppUpdates(__DEV__);
    const currentVersion = getVersion();
    const shouldForce = await getShouldForceUpdate();

    inAppUpdates.checkNeedsUpdate({ curVersion: currentVersion }).then(result => {
      if (result.shouldUpdate) {
        let updateOptions = {};
        if (shouldForce) {
          if (Platform.OS === 'android') {
            updateOptions.updateType = UpdateKind.IMMEDIATE;
          }
          if (Platform.OS === 'ios') {
            updateOptions.forceUpgrade = true;
          }
        } else {
          if (Platform.OS === 'android') {
            updateOptions.updateType = UpdateKind.FLEXIBLE;
          }
        }
        inAppUpdates.startUpdate(updateOptions);
      }
    });
  }, []);

  useEffect(() => {
    configVersion();
  }, [configVersion]);

  const appState = useRef(AppState.currentState);
  useEffect(() => {
    const subscription = AppState.addEventListener('change', async nextAppState => {
      const shouldForceUpdate = await getShouldForceUpdate();
      if (appState.current.match(/inactive|background/) && nextAppState === 'active') {
        if (shouldForceUpdate) {
          configVersion();
        }
      }
      appState.current = nextAppState;
    });

    return () => {
      subscription.remove();
    };
  }, [configVersion]);
};
