import { Dimensions } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

const { height: windowHeight, width: windowWidth } = Dimensions.get('window');

export const useWindowSize = () => {
  const insets = useSafeAreaInsets();

  const height = windowHeight - (insets.top + insets.bottom);
  const width = windowWidth - (insets.right + insets.left);

  return {
    height,
    width,
  };
};
