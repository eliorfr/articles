import React from 'react';
import PropTypes from 'prop-types';
import EStylesheet from 'react-native-extended-stylesheet';
import FastImage from 'react-native-fast-image';
import { StyleSheet, View } from 'react-native';

import avatarManImage from '../assets/avatar_man.png';
import avatarFemaleImage from '../assets/avatar_female.png';

const getStyles = size =>
  EStylesheet.create({
    fixOverlay: {
      position: 'absolute',
      zIndex: 1,
      height: size,
      width: size,
      borderRadius: size,
      borderWidth: StyleSheet.hairlineWidth,
      borderColor: 'transparent',
    },
    image: {
      height: size,
      width: size,
      borderRadius: size,
      borderWidth: StyleSheet.hairlineWidth,
      borderColor: 'transparent',
    },
  });

const ImageProfile = ({ size = 40, url = null, style = '', gender = null }) => {
  const styles = getStyles(size);
  const avatar = gender === 'female' ? avatarFemaleImage : avatarManImage;

  return (
    <View style={style}>
      <View style={styles.fixOverlay} />
      <FastImage style={styles.image} source={url ? { uri: url } : avatar} resizeMode={FastImage.resizeMode.cover} />
    </View>
  );
};

ImageProfile.propTypes = {
  url: PropTypes.string,
  gender: PropTypes.string,
  size: PropTypes.number,
  style: PropTypes.object,
};

export default ImageProfile;
