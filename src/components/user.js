import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';
import Icon from 'react-native-vector-icons/AntDesign';
import ImageProfile from './image_profile';

const styles = EStylesheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 60,
  },
  imageAndName: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  name: {
    marginLeft: 10,
    fontSize: 20,
  },
  debt: {
    fontSize: 14,
    fontWeight: '500',
    marginRight: 20,
  },
});

const User = ({ user }) => {
  const navigation = useNavigation();

  const onPress = () => {
    navigation.navigate('UserDetails', { user });
  };

  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.imageAndName}>
        <ImageProfile />
        <Text style={styles.name}>{`${user.firstName} ${user.lastName}`}</Text>
      </View>
      <View style={styles.imageAndName}>
        <Text style={styles.debt}>{`${user.DebtStatus}₪`}</Text>
        <Icon name="right" size={30} color="black" />
      </View>
    </TouchableOpacity>
  );
};

export default User;
