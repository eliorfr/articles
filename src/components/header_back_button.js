import React from 'react';
import EStylesheet from 'react-native-extended-stylesheet';
import { Keyboard, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/AntDesign';

const styles = EStylesheet.create({
  headerLeft: {
    marginLeft: '$appMargin',
  },
});

const HeaderBackButton = () => {
  const navigation = useNavigation();

  const onPress = () => {
    navigation.goBack(null);
    Keyboard.dismiss();
  };

  return (
    <TouchableOpacity onPress={onPress} style={styles.headerLeft}>
      <Icon name="left" size={36} color="black" />
    </TouchableOpacity>
  );
};

export default HeaderBackButton;
