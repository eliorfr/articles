import React, { useState } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';
import { useReduxState } from '../hooks/use_redux_state';

const styles = EStylesheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 20,
    paddingHorizontal: 24,
  },
  section: {
    width: 110,
    padding: 5,
    margin: 5,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: 'red',
  },
  isSelected: {
    backgroundColor: 'rgba(255,0,0,0.2)',
  },
});

const sections = ['Entertainment', 'General', 'Business', 'Health', 'Sport', 'Science', 'Technology'];

const Sections = () => {
  const [selectedCategory, setSelectedCategory] = useReduxState({ key: 'selectedCategory' });

  const onSelect = value => {
    setSelectedCategory(value);
    if (selectedCategory === value) {
      setSelectedCategory('');
    }
  };

  return (
    <View style={styles.container}>
      {sections.map(s => (
        <TouchableOpacity
          key={s}
          style={[styles.section, selectedCategory === s && styles.isSelected]}
          onPress={() => onSelect(s)}>
          <Text>{s}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default Sections;
