import React from 'react';

import SearchBar from './search_bar';
import { useReduxState } from '../hooks/use_redux_state';

const ArticleSearch = () => {
  const [, setSearchQuery] = useReduxState({ key: 'searchQuery' });

  const textChangedHandler = text => {
    setSearchQuery(text.trim());
  };

  return <SearchBar onChangeText={textChangedHandler} placeholder="Tap to search..." />;
};

export default ArticleSearch;
