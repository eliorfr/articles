import React, { useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { View, ActivityIndicator as ActivityIndicatorReactNative, Keyboard } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

const ActivityIndicator = ({ animating, children, hideKeyboard }) => {
  const insets = useSafeAreaInsets();

  const styles = useMemo(() => {
    return EStyleSheet.create({
      container: {
        flex: 1,
      },
      loadingContainer: {
        position: 'absolute',
        top: insets.top,
        left: 0,
        right: 0,
        bottom: insets.bottom,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 5,
        zIndex: 5,
      },
    });
  }, [insets.bottom, insets.top]);

  useEffect(() => {
    if (animating && hideKeyboard) {
      Keyboard.dismiss();
    }
  }, [animating, hideKeyboard]);

  return (
    <View style={styles.container}>
      {children}
      {animating && (
        <View style={styles.loadingContainer}>
          <ActivityIndicatorReactNative animating color="#304ffe" size="large" />
        </View>
      )}
    </View>
  );
};

ActivityIndicator.propTypes = {
  animating: PropTypes.bool.isRequired,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
  hideKeyboard: PropTypes.bool,
};

ActivityIndicator.defaultProps = {
  hideKeyboard: false,
};

export default ActivityIndicator;
