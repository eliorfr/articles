import React from 'react';
import { ActivityIndicator, TouchableOpacity, Text } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';

const styles = EStylesheet.create({
  footer: {
    backgroundColor: 'rgba(0,0,0,0.1)',
    paddingVertical: 10,
    marginVertical: 10,
    alignItems: 'center',
  },
  text: {
    fontSize: 18,
    fontWeight: '500',
  },
});

const Footer = ({ getData, loading }) => {
  return (
    <TouchableOpacity style={styles.footer} onPress={getData}>
      <Text style={styles.text}>Load More</Text>
      {loading ? <ActivityIndicator color="white" style={{ marginLeft: 8 }} /> : null}
    </TouchableOpacity>
  );
};

export default Footer;
