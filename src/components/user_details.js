import React from 'react';
import { Text, View } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';
import { PricingCard } from 'react-native-elements';
import { Divider } from 'react-native-elements';

import users from '../utils/users.json';

const styles = EStylesheet.create({
  container: {
    flex: 1,
    padding: 12,
  },
  contentContainer: {
    flexGrow: 1,
  },
});

const UserDetails = ({ route }) => {
  const { user } = route.params || {};

  return (
    <View style={styles.container}>
      <Text>{user.lastName}</Text>
      <Divider style={{ backgroundColor: 'red' }} />
      <PricingCard
        color="#4f9deb"
        title="Free"
        price="$0"
        info={['1 User', 'Basic Support', 'All Core Features']}
        button={{ title: 'GET STARTED', icon: 'flight-takeoff' }}
      />
    </View>
  );
};

export default UserDetails;
