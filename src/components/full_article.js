import React from 'react';
import { Text, ScrollView } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';
import defaultCoverImage from '../assets/default_cover_image.jpg';

import users from '../utils/users.json';
import dayjs from 'dayjs';
import FastImage from 'react-native-fast-image';

const styles = EStylesheet.create({
  container: {
    flex: 1,
    padding: 24,
  },
  contentContainer: {
    flexGrow: 1,
  },
  author: {
    fontWeight: '500',
    fontSize: 18,
    marginBottom: 8,
  },
  title: {
    fontWeight: '500',
    fontSize: 24,
    marginBottom: 8,
  },
  publishedAt: {
    fontWeight: 'bold',
    fontSize: 12,
    marginBottom: 8,
  },
  coverImage: {
    height: 80,
    width: 80,
    marginVertical: 12,
    alignSelf: 'center',
  },
  description: {
    color: 'rgba(0,0,0,0.6)',
    fontSize: 14,
  },
});

const FullArticle = ({ route }) => {
  const { article } = route.params || {};

  return (
    <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
      <Text style={styles.title}>{article.title}</Text>
      <Text style={styles.publishedAt}>{dayjs(article.publishedAt).format('MM/DD/YYYY')}</Text>
      <Text style={styles.author}>{article.author}</Text>
      <FastImage style={styles.coverImage} source={article.urlToImage ? { uri: article.urlToImage } : defaultCoverImage} />
      <Text style={styles.description}>{article.description}</Text>
      <Text style={styles.description}>{article.content}</Text>
    </ScrollView>
  );
};

export default FullArticle;
