import React from 'react';

export const navigationRef = React.createRef();
export const currentScreenRef = React.createRef();

export const navigate = (name, params) => {
  navigationRef.current?.navigate(name, params);
};

export const navigateAndReset = (...screens) => {
  const routes = screens.map(s => {
    return { name: s };
  });
  navigationRef.current?.reset({
    index: screens.length - 1,
    routes,
  });
};

export const getActiveRouteName = state => {
  if (!state) {
    return;
  }
  const route = state?.routes[state.index || 0];

  if (route.state) {
    return getActiveRouteName(route.state);
  }
  return route.name;
};

export const onStateChange = async state => {
  if (!state) {
    return;
  }
  const currentScreenName = getActiveRouteName(state);
  currentScreenRef.current = currentScreenName;
};
