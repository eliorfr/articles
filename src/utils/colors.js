export const primaryColor = '#3263E2';
export const secondaryColor = '#E1CB05';
export const errorColor = '#E70202';
export const successColor = '#33C993';
